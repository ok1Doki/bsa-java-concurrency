create table images
(
    id   uuid not null
        constraint images_pkey
            primary key,
    hash bigint,
    name varchar(255),
    path varchar(255),
    type varchar(255),
    url  varchar(255)
);

create function hamming(hash1 bigint, hash2 bigint) returns real
    language plpgsql
as
$$
DECLARE
    ones float4;
BEGIN
    ones = (select count(*)
            from (select x, generate_series(1, length(x)) as i
                  from (values ((hash1 # hash2)::bit(64))) as something(x)) as q
            where substring(x, i, 1) = B'1');
    RETURN 1.0 - (ones / 64.0);
END
$$;