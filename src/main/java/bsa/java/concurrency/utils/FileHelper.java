package bsa.java.concurrency.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
public class FileHelper {

    public static String saveImage(BufferedImage image, String filePath) throws IOException {
        File directory = new File(filePath).getParentFile();
        if (!directory.exists()) {
            System.out.println(directory.getCanonicalPath());
            directory.mkdirs();
        }

        File outputFile = new File(filePath);
        ImageIO.write(image, "png", outputFile);
        log.info(String.format("File was saved to '%s'", filePath));
        return filePath;
    }

    public static void deleteFileByPath(String filePath) {
        File file = new File(filePath);

        if (file.delete()) {
            log.info(String.format("File '%s' deleted successfully", filePath));
        } else {
            log.error(String.format("Failed to delete file '%s'", filePath));
        }
    }

    public static void clearDirectory(String path) throws IOException {
        File file = new File(path);

        if (file.isDirectory()) {
            FileUtils.cleanDirectory(file);
            log.info(String.format("Directory '%s' was successfully cleared", path));
        } else {
            log.error(String.format("Failed to clear directory '%s'", path));
        }
    }
}
