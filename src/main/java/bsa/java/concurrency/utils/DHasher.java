package bsa.java.concurrency.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

public class DHasher {

    public static long calculateHash(byte[] image) {
        try {
            long hash = 0;

            BufferedImage img = ImageIO.read(new ByteArrayInputStream(image));
            BufferedImage preparedImage = prepareImage(img);

            for (int row = 1; row < 9; row++) {
                for (int col = 1; col < 9; col++) {
                    hash |= preparedImage.getRGB(col, row) > preparedImage.getRGB(col - 1, row - 1)
                            ? 1
                            : 0;
                    hash <<= 1;
                }
            }

            return hash;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static BufferedImage prepareImage(BufferedImage image) {
        var scaledImage = image.getScaledInstance(9, 9, Image.SCALE_SMOOTH);
        var grayscaleImage = new BufferedImage(9, 9, BufferedImage.TYPE_BYTE_GRAY);

        Graphics2D g2d = grayscaleImage.createGraphics();
        g2d.drawImage(scaledImage, 0, 0, null);
        g2d.dispose();

        return grayscaleImage;
    }
}
