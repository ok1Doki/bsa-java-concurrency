package bsa.java.concurrency.image;

import bsa.java.concurrency.exception.exceptions.ImageNotFoundException;
import bsa.java.concurrency.exception.exceptions.IncorrectRequestException;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
@Slf4j
@RequestMapping("/image")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] images) throws IOException, IncorrectRequestException {
        log.info(String.format("POST /image/batch - with %d images", images.length));

        if (images.length == 0) {
            throw new IncorrectRequestException("There are no images in the request body");
        }

        try {
            for (MultipartFile file : images) {
                ImageData imageData = ImageData.builder()
                        .type(file.getContentType())
                        .name(file.getOriginalFilename())
                        .build();
                imageService.uploadImage(imageData, file.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(
            @RequestParam("image") MultipartFile file,
            @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) throws IOException {
        ImageData imageData = ImageData.builder()
                .type(file.getContentType())
                .name(file.getOriginalFilename())
                .build();

        List<SearchResultDTO> result = imageService.findImages(file.getBytes(), threshold).orElse(Collections.emptyList());
        if (result.isEmpty()) {
            imageService.uploadImage(imageData, file.getBytes());
        }
        return result;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) throws ImageNotFoundException {
        imageService.deleteImage(imageId);
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages() throws IOException {
        imageService.deleteAllImages();
    }
}
