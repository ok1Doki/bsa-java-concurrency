package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<ImageData, UUID> {

    @Query(value = "SELECT " +
            "cast(i.id as varchar) AS \"ImageId\", " +
            "hamming(i.hash, :hash) AS \"MatchPercent\", " +
            "i.path || i.name AS \"ImageUrl\" " +
            "FROM images i " +
            "WHERE hamming(i.hash, :hash) > :threshold ",
            nativeQuery = true)
    List<SearchResultDTO> findImage(
            @Param("hash") long hash,
            @Param("threshold") double threshold
    );


}