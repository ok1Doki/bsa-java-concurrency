package bsa.java.concurrency.image;

import bsa.java.concurrency.exception.exceptions.ImageNotFoundException;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.utils.DHasher;
import bsa.java.concurrency.utils.FileHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
public class ImageService {

    @Value("${app.image.location}")
    private String STORAGE_PATH;

    @Autowired
    private ImageRepository imageRepository;

    @Async
    public CompletableFuture<String> uploadImage(ImageData imageData, byte[] fileBytes) throws IOException {
        String fileName = imageData.getName();
        String rootPath = new File(".").getCanonicalPath();
        String directoryPath = rootPath + STORAGE_PATH;
        String fullPath = directoryPath + fileName;

        log.info(String.format("Start processing '%s' with size %d", fileName, fileBytes.length));

        return CompletableFuture
                .allOf(
                        calculateHash(fileBytes)
                                .thenApply(hash -> {
                                    log.info(String.format("Hash of '%s' = %d", fileName, hash));
                                    imageData.setHash(hash);
                                    return hash;
                                }),
                        saveFile(fullPath, fileBytes)
                                .thenApply(path -> {

                                    imageData.setPath(directoryPath);
                                    return path;
                                }))
                .thenApply(v ->
                        saveFileMetaData(imageData).join());
    }

    @Async
    public CompletableFuture<Long> calculateHash(byte[] fileBytes) {
        return CompletableFuture.supplyAsync(() -> DHasher.calculateHash(fileBytes));
    }

    @Async
    public CompletableFuture<String> saveFileMetaData(ImageData imageData) {
        return CompletableFuture.supplyAsync(() -> {
            imageRepository.save(imageData);
            return imageData.getPath();
        });
    }

    @Async
    public CompletableFuture<String> saveFile(String path, byte[] file) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(file));
                return FileHelper.saveImage(bufferedImage, path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    public Optional<List<SearchResultDTO>> findImages(byte[] bytes, double threshold) {
        Optional<List<SearchResultDTO>> result = Optional.of(imageRepository.findImage(calculateHash(bytes).join(), threshold));
        System.out.println(result.get().toString());
        return result;
    }

    public void deleteImage(UUID imageId) throws ImageNotFoundException {
        String imagePath = imageRepository
                .findById(imageId)
                .map(imageData -> imageData.getPath() + imageData.getName())
                .orElseThrow(
                        () -> {
                            try {
                                throw new ImageNotFoundException(imageId);
                            } catch (ImageNotFoundException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                );
        FileHelper.deleteFileByPath(imagePath);
        imageRepository.deleteById(imageId);
    }

    public void deleteAllImages() throws IOException {
        String root = new File(".").getCanonicalPath();
        FileHelper.clearDirectory(root + STORAGE_PATH);
        imageRepository.deleteAll();
    }
}