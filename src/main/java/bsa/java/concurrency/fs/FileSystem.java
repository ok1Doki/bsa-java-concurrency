package bsa.java.concurrency.fs;

import java.util.concurrent.CompletableFuture;

public interface FileSystem {

    CompletableFuture<String> saveFile(String path, byte[] file);

}
