package bsa.java.concurrency.exception;

import bsa.java.concurrency.api.ErrorResponse;
import bsa.java.concurrency.exception.exceptions.ImageNotFoundException;
import bsa.java.concurrency.exception.exceptions.IncorrectRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorResponse> handleAllExceptions(Exception ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorResponse error = new ErrorResponse(status.getReasonPhrase(), details);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(IncorrectRequestException.class)
    public final ResponseEntity<ErrorResponse> handleUserAlreadyExistsException(IncorrectRequestException ex,
                                                                                WebRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ErrorResponse error = new ErrorResponse(
                status.getReasonPhrase(),
                Collections.singletonList(ex.getLocalizedMessage())
        );
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler(ImageNotFoundException.class)
    public final ResponseEntity<ErrorResponse> handleUserAlreadyExistsException(ImageNotFoundException ex,
                                                                                WebRequest request) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        ErrorResponse error = new ErrorResponse(
                status.getReasonPhrase(),
                Collections.singletonList(ex.getLocalizedMessage())
        );
        return new ResponseEntity<>(error, status);
    }
}