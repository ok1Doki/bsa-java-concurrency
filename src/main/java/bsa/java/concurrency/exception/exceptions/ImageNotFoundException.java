package bsa.java.concurrency.exception.exceptions;

import java.util.UUID;

public class ImageNotFoundException extends Exception {
    public ImageNotFoundException(UUID imageId) {
        super(String.format("Image with id '%s' not found!", imageId));
    }
}
