package bsa.java.concurrency.exception.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IncorrectRequestException extends Exception {

    public IncorrectRequestException(String message) {
        super(message);
        log.error(message);
    }

}
